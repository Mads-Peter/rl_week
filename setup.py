from setuptools import setup, find_packages

version = "0.0.1"

setup(
    name="rlweek",
    version=version,
    description="rlweek.",
    python_requires=">=3.5",
    packages=find_packages(),
    install_requires=[
        "numpy",
        "torch",
        "gymnasium[toy_text]",
        "matplotlib",
        "tqdm",
        "ipykernel",
        "jupyter",
        "pandas",
        "schnetpack",
        "urllib3==1.26.6",        
        "agox @ git+https://gitlab.com/agox/agox.git@dev"    
    ]
)
