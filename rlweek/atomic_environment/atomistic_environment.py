from ase import Atoms
from agox.generators import RandomGenerator
import numpy as np
import gymnasium as gym
from gymnasium.spaces import Box, Graph
from gymnasium.spaces.graph import GraphInstance
from scipy.spatial.distance import cdist


class AtomisticEnvironment(gym.Env):
    """
    Gymnasium environment for atomistic optimization.

    Parameters
    ----------
    env : agox.environments.Environment
        The AGOX environment to base the environment on.
    calc : ase.calculators.Calculator
        The calculator to use for the energy calculations.
    n_steps : int
        The number of steps to run for in an episode.
    """
    def __init__(self, env, calc, n_steps=10):
        self.calculator = calc
        self.agox_env = env
        self.n_steps = n_steps

        # Observation space is the descriptor of the atoms. 
        confinement_cell = self.agox_env.get_confinement()['confinement_cell']
        x_size = confinement_cell[0, 0]
        y_size = confinement_cell[1, 1]
        self.size = np.array([x_size, y_size])
        self.offset = self.agox_env.get_confinement()['confinement_corner'][0:2]

        # The observation space is a graph with node features of position and type.
        # This is a bit hacky, because the type is given as a float, so 
        # samples are drawn from the space it needs to be converted to an int, 
        # by rounding.
        n_atom_types = len(np.unique(self.agox_env.get_all_numbers()))
        node_space = Box(low=np.array([0.0, 0.0, 0]), high=np.array([x_size, y_size, n_atom_types]), dtype=np.float64, shape=(3,))
        self.observation_space = Graph(node_space=node_space, edge_space=None)
        
        # The action space is a composite of the discrete atom index and a box for the position.
        n_atoms = len(self.agox_env.get_all_numbers())
        self.action_space = gym.spaces.Tuple((gym.spaces.Discrete(n_atoms), 
                                              gym.spaces.Box(
                                                low=np.array([0.0, 0.0]), 
                                                high=np.array([x_size, y_size]), 
                                                shape=(2,), dtype=np.float64
                                                )))

    def initial_state(self):
        """
        Generate a random initial state using the AGOX Random Generator. 

        Returns
        -------
        atoms : ase.Atoms
        """
        # Initial state is a random configuration of atoms.
        generator = RandomGenerator(**self.agox_env.get_confinement(), c1=0.75, c2=3)
        state = generator(None, self.agox_env)[0]
        return state

    def step(self, action):
        """
        Perform a step in the environment.

        Parameters
        ----------
        action : tuple
            The action is a tuple of (atom_index, x, y, z) describing the final 
            position of the atom.
        
        Returns
        -------
        state : gymnasium.spaces.GraphInstance
            The state observation of the environment.
        reward : float
            The reward for the action.
        terminal : bool
            Whether the episode is over.
        truncated : bool
            Whether the episode was truncated.
        info : dict
            Additional information.           
        """

        # Make the action 'safe' - cant move outside the confinement cell.
        index = np.clip(action[0], 0, len(self.atoms)-1)
        position = np.clip(action[1], 0, self.size)

        # The new positions: 
        positions = self.atoms.positions.copy() 
        positions[index, 0:2] = position + self.offset
        if self.check_positions(positions):
            # An action is a 4-tuple of (atom_index, x, y, z) describing the final 
            # position of the atom. 
            self.atoms.positions[index, 0:2] = position + self.offset

        state = self.get_state_observation(self.atoms)

        # The reward is the difference between the previous energy and the current energy.
        energy = self.get_energy(self.atoms)
        reward = -(energy - self.previous_energy)

        # Save the energy for the next step.
        self.previous_energy = energy

        # Terminal and stuff
        self.steps += 1
        terminal = self.steps >= self.n_steps
        truncated = False
        info = {}

        return state, reward, terminal, truncated, info

    def reset(self):
        """
        Reset the environment to a random initial state.
        """
        self.atoms = self.initial_state()
        self.steps = 0
        self.previous_energy = self.get_energy(self.atoms)
        observation = self.get_state_observation(self.atoms)
        return observation, {}
    
    def get_state_observation(self, atoms):
        """
        Get the state observation from the atoms.

        Parameters
        ----------
        atoms : ase.Atoms
            The atoms to get the state observation from.        
        """
        types = atoms.get_atomic_numbers()
        unique_types = np.unique(types)
        types = np.array([np.where(unique_types == t)[0] for t in types]).reshape(-1, 1)
        positions = atoms.get_positions()[:, 0:2] - self.offset
        node_features = np.hstack([positions, types])

        return GraphInstance(nodes=node_features, edges=None, edge_links=None)

    def get_energy(self, atoms):
        """
        Get the energy of the atoms.

        Parameters
        ----------
        atoms : ase.Atoms
            The atoms to calculate the energy for.
        """
        atoms.calc = self.calculator
        energy = atoms.get_potential_energy()
        return energy

    def check_positions(self, positions):        
        distances = cdist(positions, positions)
        distances += np.eye(positions.shape[0])

        return np.all(distances > 0.25) # Not ideal, but work for now. 

