import numpy as np
from rlweek.atomic_environment import (
    AtomisticEnvironment,
    get_agox_environment,
    get_lennard_jones_calculator,
)
from tqdm import tqdm
from rlweek.atomic_environment.wrappers import DiscretizationWrapper


def evaluate_agent(agent, episodes=100, n_atoms=5, size=5, edge_width=2,
                   verbose=True, discretize=False, grid_size=10):
    
    # Setup the AGOX environment:
    agox_environment = get_agox_environment(
        symbols=f"H{n_atoms}", size=size, edge_width=edge_width
    )

    # Calculator:
    calc = get_lennard_jones_calculator()

    # Setup the gym environment:
    env = AtomisticEnvironment(agox_environment, calc)

    if discretize:
        env = DiscretizationWrapper(env, grid_size=grid_size)

    # If the agent needs to set something based on the environment.
    agent.set_env(env)

    returns = np.zeros(episodes)

    for episode in tqdm(range(episodes)):
        state, _ = env.reset()
        terminal = False

        while not terminal:
            action = agent.get_action(state)
            state, reward, terminal, _, _ = env.step(action)

            returns[episode] += reward

    improved = returns > 0
    mean_improvement = np.mean(returns[improved])
    mean_decay = np.mean(returns[~improved])

    stats = {
            'mean_return': np.mean(returns),
            'median_return': np.median(returns),
            'std_return': np.std(returns),
            'improvement_percentage': np.sum(returns > 0) / episodes * 100,
            'returns': returns,
            'mean_improvement': mean_improvement,
            'mean_decay': mean_decay,        
            }

    if verbose:
        for key in stats:
            if key != 'returns':
                print(f"{key.capitalize()}: {stats[key]}")

    return stats