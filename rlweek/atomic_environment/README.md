# Atomistic Reinforcement Learning. 

The `tutorial05_atomistic_environment.ipynb` explores a RL environment 
for atomistic optimization. 

A few strategy proposals for solving this environment are given. I hope that 
for the remainder of the bootcamp we can work on this environment, so to facilitate 
we will 'compete' at building agents that are best at solving this environment. 

Traditionally for optimization that would involve using sucess curves to measure 
an optimizations algorithms ability to consistently find the global optimum solution. 
However, we will adopt the RL approach of caring about maximizing the return as
that is the explicit goal of an RL algorithm. 

This directory contains a few files in addition to the two notebooks, 

- atomistic_environment.py: Contains the definition of the `AtomisticEnvironment`, 
look through while working through `tutorial05`. 
- helpers.py: Contains some helper functions that you might find useful. 
- evaluate_agent.py: Code to evaluate an agent on the `AtomisticEnvironment`. 
- The agents directory contains the three simple non-RL agents I have implemented, 
alongside a very minimal `AgentBaseClass`. Your agent should inherit from this 
to make sure it works with the evaluation code. 
- wrappers.py: Wrappers to modify the environment, currently just contains a 
wrapper to make a discretized version of the environment. If you come up with 
more useful wrappers put them here. 

## The Competition: 

Design a (preferably RL-based) agent to solve the `AtomisticEnvironment`. 
I have outlined some ideas in `tutorial05` but you are also more than encouraged 
to come up with your own approach. 

We will keep score of all the agents. 

All tools allowed, if you can get any of the RL software libraries described in 
the main README to work on this environment - that would be fantastic. 

Want to implement a simple algorithm but just crunch through many training episodes? 
Grab a GPU or try using [Vectorized Environments](https://gymnasium.farama.org/api/vector/). Or both. 

The default evaluate agent script does so for a LennardJones potential with just 5 atoms, 
if you want something more complicated you can replace the calculator and change the stoichoimetry, but I suggest
working on this easy'ish version before you do so. 



