import numpy as np
import torch
from agox.candidates import StandardCandidate
from agox.generators import RattleGenerator
from agox.samplers import SamplerBaseClass
from scipy.spatial.distance import cdist
from tqdm import tqdm

from rlweek.atomic_environment import graph_to_atoms
from rlweek.atomic_environment.agents import AgentBaseClass
from rlweek.atomic_environment.agents.rattle_agent import (
    DummySampler, SingleAtomRattleGenerator)

if torch.cuda.is_available():
    device = torch.device("cuda")
else:
    device = torch.device("cpu")


class Trajectory:
    def __init__(self):
        self.states = []
        self.actions = []
        self.rewards = []
        self.log_probs = []

    def append(self, state, action, reward, log_prob):
        self.states.append(state)
        self.actions.append(action)
        self.rewards.append(reward)
        self.log_probs.append(log_prob)

    def __len__(self):
        return len(self.states)


class PolicyNetwork(torch.nn.Module):
    def __init__(self, obs_size, n_actions, hidden_size=32, hidden_layers=1, lr=1e-3):
        super(PolicyNetwork, self).__init__()

        layers = []
        layers.append(torch.nn.Linear(obs_size, hidden_size))
        layers.append(torch.nn.ReLU())
        
        for _ in range(hidden_layers):
            layers.append(torch.nn.Linear(hidden_size, hidden_size))
            layers.append(torch.nn.ReLU())
            
        layers.append(torch.nn.Linear(hidden_size, n_actions))

        self.model = torch.nn.Sequential(*layers)

        self.n_actions = n_actions
        self.optimizer = torch.optim.Adam(self.parameters(), lr=lr)

    def forward(self, x):
        return self.model(x)

    def get_probabilities(self, x):
        # Calculate action preferences:
        prefs = self.forward(x)
        return torch.nn.functional.softmax(prefs, dim=0)

    def sample_action(self, x):
        probs = self.get_probabilities(x).view(-1)
        action = torch.distributions.Categorical(probs).sample()
        return action, torch.log(probs[action])


class SimpleAgent(AgentBaseClass):
    def __init__(
        self,
        n_samples,
        policy,
        descriptor,
        c1=0.75,
        c2=1.25,
        device=torch.device("cpu"),
        **kwargs
    ):
        super().__init__(**kwargs)
        self.c1 = c1  # This has a large influence on the performance.
        self.n_samples = n_samples

        self.policy = policy
        self.policy.to(device)

        self.descriptor = descriptor
        self.device = device

    def set_env(self, env):
        self.agox_env = env.agox_env
        self.env = env

        # Setup a random generator:
        self.generator = SingleAtomRattleGenerator(
            **env.agox_env.get_confinement(), c1=self.c1, c2=1.25
        )
        self.sampler = DummySampler()
        self.offset = env.agox_env.get_confinement()["confinement_corner"][0:2]

    def generate(self, state):
        # Run the generator:
        atoms = graph_to_atoms(state, self.agox_env)
        template = self.agox_env.get_template()
        candidate = StandardCandidate.from_atoms(template, atoms)

        all_atoms = []
        indexs = []
        new_positions = []
        # pick atom index at random
        index = np.random.randint(0, len(atoms))
        self.generator.index = index
        
        for n in range(self.n_samples):
            if n == 0:
                new_position = atoms.positions[index, 0:2]
                all_atoms.append(atoms)
                indexs.append(index)
                new_positions.append(new_position - self.offset)
                continue

            # Update sampler:
            self.sampler.setup([candidate.copy()])
            rattled_atoms = self.generator(self.sampler, self.agox_env)[0]

            # Extract the action:
            index = self.generator.rattled_index
            new_position = rattled_atoms.positions[index, 0:2] - self.offset
            all_atoms.append(rattled_atoms)
            indexs.append(index)
            new_positions.append(new_position)

        return indexs, new_positions, all_atoms

    def get_rattle_state(self, indexs, new_positions, atoms):
        # this should be some feature vector of the state
        state = []
        for i in range(self.n_samples):
            f = self.descriptor.get_features(atoms[i])[indexs[i]]
            state.append(f)

        return torch.tensor(np.array(state), dtype=torch.float32).to(self.device)

    def get_action(self, state):
        indexs, new_positions, atoms = self.generate(state)
        rattle_state = self.get_rattle_state(indexs, new_positions, atoms)

        # idx = np.random.randint(0, self.n_samples)
        idx, _ = self.policy.sample_action(rattle_state)
        action = [indexs[idx], new_positions[idx]]

        return action

    def play_episode(self):
        state, _ = self.env.reset()
        terminal = False
        truncated = False

        trajectory = Trajectory()
        while not terminal and not truncated:
            # Get the action and the corresponding log_prob from the policy.
            # Remember to convert the state to a torch tensor.
            indexs, new_positions, atoms = self.generate(state)
            rattle_state = self.get_rattle_state(indexs, new_positions, atoms)
            idx, log_probs = self.policy.sample_action(rattle_state.to(self.device))
            action = [indexs[idx], new_positions[idx]]

            # Take a step in the environment with the action:
            next_state, reward, terminal, truncated, info = self.env.step(action)

            # Append the step to the trajectory:
            trajectory.append(state, action, reward, log_probs)

            # Update the state:
            state = next_state

        return trajectory

    def update(self, trajectory, gamma):
        T = len(trajectory)

        # Calculate G_t for each time step
        G = []
        for t in range(T):
            Gt = np.sum([trajectory.rewards[k] * gamma ** (k - t) for k in range(t, T)])
            G.append(Gt)

        G = torch.tensor(np.array(G)).to(self.device)

        # Calculate the gradients of the log-prob
        log_probs_tensor = torch.stack(trajectory.log_probs).to(self.device)
        loss = -log_probs_tensor * G

        # Update the policy
        self.policy.optimizer.zero_grad()
        loss.sum().backward()
        self.policy.optimizer.step()

    def train(self, episodes=100, gamma=0.99):
        returns = np.zeros(episodes)

        for episode in tqdm(range(episodes)):
            # Play an episode:
            trajectory = self.play_episode()

            # Update the policy:
            self.update(trajectory, gamma)

            # Print the total reward of the episode:
            returns[episode] = np.sum(trajectory.rewards)

        return returns


if __name__ == "__main__":
    pass
