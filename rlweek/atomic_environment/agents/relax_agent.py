import numpy as np
from rlweek.atomic_environment.agents import AgentBaseClass
from rlweek.atomic_environment import graph_to_atoms
from ase.optimize import QuasiNewton, BFGS
from ase.constraints import FixAtoms, FixedPlane


class RelaxAgent(AgentBaseClass):

    def set_env(self, env):
        self.calc = env.calculator
        self.agox_env = env.agox_env
        self.offset = env.offset

    def get_action(self, state):
        
        # Atoms.
        atoms = graph_to_atoms(state, self.agox_env)
        atoms.calc = self.calc

        # Choose one index to NOT fix. 
        index = np.random.randint(low=0, high=len(atoms))
        fixed_indices = [i for i in range(len(atoms)) if i != index]

        constraint0 = FixAtoms(indices=fixed_indices)
        constraint1 = FixedPlane([index], [0, 0, 1])

        atoms.set_constraint([constraint0, constraint1])
        atoms.calc = self.calc

        # Relax.
        opt = BFGS(atoms, logfile=None)
        opt.run(fmax=0.1, steps=50)

        # Extract the action: 
        new_position = atoms.positions[index, 0:2]
        new_position = new_position - self.offset
        action = [index, new_position[0], new_position[1]]

        return (index, new_position[0:2])

if __name__ == '__main__':

    from rlweek.atomic_environment import evaluate_agent

    agent = RelaxAgent()
    evaluate_agent(agent, episodes=100)

    # from rlweek.atomic_environment import evaluate_agent, AtomisticEnvironment, get_agox_environment, get_lennard_jones_calculator

    # # Environment:
    # n_atoms = 4
    # size = 5
    # edge_width = 0
    # agox_environment = get_agox_environment(
    #     symbols=f"H{n_atoms}", size=size, edge_width=edge_width
    # )

    # # Calculator:
    # calc = get_lennard_jones_calculator()

    # # Setup the gym environment:
    # env = AtomisticEnvironment(agox_environment, calc)

    # agent = RelaxAgent()
    # agent.set_env(env)

    # state, _ = env.reset()
    # action = agent.get_action(state)
    # state, reward, _, _, _ = env.step(action)

    # atoms = graph_to_atoms(state, agox_environment)
    # atoms.calc = calc
    # print(atoms.get_potential_energy())

    # print(atoms.positions)

