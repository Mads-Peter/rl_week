from rlweek.atomic_environment.agents import AgentBaseClass

class RandomAgent(AgentBaseClass):

    def set_env(self, env):
        self.action_space = env.action_space

    def get_action(self, state):
        return self.action_space.sample()


if __name__ == '__main__':

    from rlweek.atomic_environment import evaluate_agent

    agent = RandomAgent()
    evaluate_agent(agent, episodes=10)
