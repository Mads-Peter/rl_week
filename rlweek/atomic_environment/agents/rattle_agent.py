import numpy as np
from agox.generators import RattleGenerator
from agox.samplers import SamplerBaseClass
from agox.candidates import StandardCandidate
from rlweek.atomic_environment.agents import AgentBaseClass
from rlweek.atomic_environment import graph_to_atoms
from scipy.spatial.distance import cdist

class SingleAtomRattleGenerator(RattleGenerator):

    name = 'SingleAtomRattle'

    def __init__(self, *args, **kwargs):
        self.index = None
        super().__init__(*args, **kwargs)
        
    def get_indices_to_rattle(self, atoms):
        if self.index == None:
            index = np.random.randint(low=0, high=len(atoms))
        else:
            index = self.index
        
        self.rattled_index = index # Slightly hacky.
        return [index]

class DummySampler(SamplerBaseClass):

    name = 'DummySampler'

    def get_random_member(self):
        return self.sample[0]

    def setup(self, candidates):
        self.sample = [candidates[0]]

class RattleAgent(AgentBaseClass):

    def __init__(self, c1=0.75, c2=1.25, **kwargs):
        super().__init__(**kwargs)
        self.c1 = c1 # This has a large influence on the performance.        

    def set_env(self, env):
        self.agox_env = env.agox_env

        # Setup a random generator: 
        self.generator = SingleAtomRattleGenerator(**env.agox_env.get_confinement(), c1=self.c1, c2=1.25)
        self.sampler = DummySampler()
        self.offset = env.agox_env.get_confinement()['confinement_corner'][0:2]

    def get_action(self, state):

        # Convert graph to atoms and to candidate. 
        atoms = graph_to_atoms(state, self.agox_env)

        template = self.agox_env.get_template()
        candidate = StandardCandidate.from_atoms(template, atoms)

        # Update sampler:
        self.sampler.setup([candidate.copy()])

        # Run the generator:
        rattled_atoms = self.generator(self.sampler, self.agox_env)[0]

        # Extract the action: 
        index = self.generator.rattled_index
        new_positions = rattled_atoms.positions[index, 0:2] - self.offset
        action = [index, new_positions]

        return action

if __name__ == '__main__':

    from rlweek.atomic_environment import evaluate_agent, AtomisticEnvironment, get_agox_environment, get_lennard_jones_calculator

    # Environment:
    n_atoms = 4
    size = 5
    edge_width = 10
    agox_environment = get_agox_environment(
        symbols=f"H{n_atoms}", size=size, edge_width=edge_width
    )

    # Calculator:
    calc = get_lennard_jones_calculator()

    # Setup the gym environment:
    env = AtomisticEnvironment(agox_environment, calc)

    # Create the agent:
    agent = RattleAgent()
    agent.set_env(env)

    state, _ = env.reset()

    atoms = graph_to_atoms(state, env.agox_env)
    print(atoms.positions)

    print('Dists before', (atoms.get_all_distances()+np.eye(4)*10).min())
    print('Energy before', env.get_energy(atoms))

    done = False
    G = 0
    while not done:
        action = agent.get_action(state)
        state, reward, done, _, info = env.step(action)

        G += reward

    atoms = graph_to_atoms(state, env.agox_env)
    print('Dists after', (atoms.get_all_distances()+np.eye(4)*10).min())
    print('Energy after', env.get_energy(atoms))

    print(atoms.positions)

    print('Return', G)

    # Evaluate the agent:
    evaluate_agent(agent, episodes=100)



