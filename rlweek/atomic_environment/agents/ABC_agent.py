from abc import ABC, abstractmethod
import gymnasium as gym

class AgentBaseClass(ABC):

    def __init__(self):
        pass

    @abstractmethod
    def get_action(state):
        pass

    def set_env(self, env):
        pass
