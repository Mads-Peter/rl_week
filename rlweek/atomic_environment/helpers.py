import numpy as np
from agox.environments import Environment
from ase import Atoms
from ase.data import covalent_radii

def get_agox_environment(symbols='H5', size=5, edge_width=2):
    # Setup the AGOX environment: 
    cell = np.eye(3) * (size + edge_width * 2)
    confinement_cell = np.eye(3) * size
    confinement_cell[2, 2] = 0
    confinement_corner = np.array([edge_width, edge_width, 0])

    template = Atoms('', cell=cell)

    agox_env = Environment(symbols=symbols, template=template,
                        confinement_cell=confinement_cell,
                        confinement_corner=confinement_corner, 
                        print_report=False)

    return agox_env

def get_lennard_jones_calculator(atomic_number=1):
    from ase.calculators.lj import LennardJones
    rmin = 2 * covalent_radii[atomic_number]
    sigma = rmin / 2**(1/6)
    calc = LennardJones(epsilon=1, sigma=sigma)

    return calc

def graph_to_atoms(graph, agox_env):
    """
    Convert a GraphInstance to an ase.Atoms object.
    """
    node_features = graph.nodes
    positions = node_features[:, 0:2].copy()
    positions = np.hstack([positions, np.zeros((len(positions), 1))])

    confinement_corner = agox_env.get_confinement()['confinement_corner']
    positions += confinement_corner

    types = node_features[:, 2].copy()
    types = types.astype(int)
    atomic_numbers = np.sort(np.unique(agox_env.get_all_numbers()))[types]

    atoms = agox_env.get_atoms()

    atoms.set_positions(positions)
    atoms.set_atomic_numbers(atomic_numbers)
    return atoms