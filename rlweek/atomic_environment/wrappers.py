import numpy as np
from gymnasium import ObservationWrapper
from gymnasium.spaces import Box
from gymnasium.spaces.graph import GraphInstance

from rlweek.atomic_environment import graph_to_atoms
from ase.symbols import symbols2numbers

class DiscretizationWrapper(ObservationWrapper):

    def __init__(self, env, grid_size=10):
        super().__init__(env)
        self.grid_size = grid_size
        self.agox_env = env.agox_env

        self.species = self.agox_env.get_species()
        self.atomic_numbers = np.sort(np.unique(symbols2numbers(self.species)))


        self.confinement_cell = self.agox_env.get_confinement()['confinement_cell']
        self.confinement_corner = self.agox_env.get_confinement()['confinement_corner']

        assert self.confinement_cell[0, 0] == self.confinement_cell[1, 1], 'Confinement cell is not a square'
        self.grid_spacing = self.confinement_cell[0, 0] / self.grid_size

        # Define the new observation space: 
        grid_shape = (self.grid_size, self.grid_size, len(self.atomic_numbers))
        self.observation_space = Box(low=0, high=1, shape=grid_shape, dtype=int)

    def observation(self, observation):

        # Positions: 
        positions = observation.nodes[:, 0:2]

        # Discretize positions:
        grid_coords = (self.snap_to_grid(positions) // self.grid_spacing).astype(int)

        # Numbers: 
        number_index = observation.nodes[:, 2].astype(int) # Atomic numbers as 0, 1, 2... Lowest number is lowest atomic number, so if we have 3 species, we have 0, 1, 2. 

        grid = np.zeros(self.observation_space.shape, dtype=int)

        for i, n in zip(range(grid_coords.shape[0]), number_index):
            grid[grid_coords[i, 0], grid_coords[i, 1], n] += 1
        
        return grid

    def grid_to_graph(self, grid):
        # Find where there is a one:
        indices = np.array(np.where(grid == 1)).T

        # Get the positions:
        positions = self.indices_to_positions(indices[:, 0:2])
        atomic_number_indices = indices[:, 2]

        node_features = np.hstack((positions, atomic_number_indices[:, None]))

        return GraphInstance(nodes=node_features, edges=None, edge_links=None)

    def graph_to_grid(self, graph):
        # Positions: 
        positions = graph.nodes[:, 0:2]

        # Discretize positions:
        grid_coords = np.array((positions + self.grid_spacing / 2) // self.grid_spacing, dtype=int)

        grid = np.zeros(self.observation_space.shape, dtype=int)

        for i in range(grid_coords.shape[0]):
            grid[grid_coords[i, 0], grid_coords[i, 1]] += 1

        return grid    

    def snap_to_grid(self, positions):
        return np.array((positions) // self.grid_spacing, dtype=int) * self.grid_spacing + self.grid_spacing / 2

    def indices_to_positions(self, grid_indices: np.ndarray):
        positions = grid_indices * self.grid_spacing + self.grid_spacing / 2
        return positions

    def reset(self):
        self.env.unwrapped.atoms = self.unwrapped.initial_state()

        self.env.unwrapped.atoms.positions = self.snap_to_grid(self.atoms.positions)
        self.env.unwrapped.steps = 0
        self.env.unwrapped.previous_energy = self.unwrapped.get_energy(self.atoms)

        graph_obs = self.unwrapped.get_state_observation(self.atoms)
        grid_obs = self.observation(graph_obs)

        return grid_obs, {}


if __name__ == '__main__':

    from rlweek.atomic_environment import (
    AtomisticEnvironment,
    get_agox_environment,
    get_lennard_jones_calculator,
    )

    # Setup the AGOX environment:
    n_atoms = 1
    size = 2
    edge_width = 0
    agox_environment = get_agox_environment(
        symbols=f"CH{n_atoms}", size=size, edge_width=edge_width
    )

    # Calculator:
    calc = get_lennard_jones_calculator()

    # Setup the gym environment:
    env = DiscretizationWrapper(AtomisticEnvironment(agox_environment, calc), grid_size=5)

    #state, _ = env.reset()

    print('Here ------------')    
    atoms = env.unwrapped.initial_state()

    positions = atoms.positions.copy()
    snapped_positions = env.snap_to_grid(positions)
    atoms.positions = snapped_positions
    graph = env.unwrapped.get_state_observation(atoms)
    print(graph)
    
    grid = env.observation(graph)

    print(grid[:, :, 0])
    print(grid[:, :, 1])

    print('Here ------------')

    graph = env.grid_to_graph(grid)
    print(graph)

    atoms_after = graph_to_atoms(graph, env.unwrapped.agox_env)





    # print(positions)
    # print(snapped_positions)






