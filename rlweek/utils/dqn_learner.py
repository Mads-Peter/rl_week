import matplotlib
matplotlib.use('Agg')
import gymnasium as gym
import torch
from typing import Any
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
from tqdm import tqdm

class Q_network(torch.nn.Module):

    def __init__(self, observation_dim, action_dim, learning_rate=1e-3, 
                 hidden_dim=16, hidden_layers=1, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        
        self.input_layer = torch.nn.Linear(observation_dim, hidden_dim) # YOUR CODE HERE
        self.hidden_layers = []
        for hidden_layer in range(hidden_layers):
            layer = torch.nn.Linear(hidden_dim, hidden_dim) # YOUR CODE HERE.
            self.hidden_layers.append(layer)  # YOUR CODE HERE.
            self.add_module(f'hidden_layer_{hidden_layer}', layer) # This lets torch now about this layer. 
        self.output_layer = torch.nn.Linear(hidden_dim, action_dim) # YOUR CODE HERE. 

        self.optimizer = torch.optim.Adam(self.parameters(), lr=learning_rate)

    def forward(self, x):

        x = self.input_layer(x)
        x = torch.relu(x)

        for hidden_layer in self.hidden_layers:
            x = hidden_layer(x)
            x = torch.relu(x)

        x = self.output_layer(x)

        return x

class ExperienceReplay: 

    def __init__(self, observation_dim, size=100, batch_size=16, device=torch.device('cpu')):
        self.batch_size = batch_size
        self.obs_dim = observation_dim
        self.size = size
        self.device = device
        self.clear()

    def add(self, state, action, reward, new_state, terminal):
        self.states[self.index] = torch.tensor(np.array(state))
        self.actions[self.index] = torch.tensor(action)
        self.rewards[self.index] = torch.tensor(reward)
        self.new_states[self.index] = torch.tensor(np.array(new_state))
        self.terminal[self.index] = torch.tensor(terminal)

        self.index += 1
        if self.index == self.size:
            self.index = 0

        if self.current_size < self.size:
            self.current_size += 1

    def get_batch(self):
        indices = np.random.randint(0, self.current_size, size=self.batch_size)
        return self.states[indices], self.actions[indices], self.rewards[indices], self.new_states[indices], self.terminal[indices]
    
    def clear(self):
        self.states = torch.tensor(np.zeros((self.size, self.obs_dim)), dtype=torch.float32, device=device) 
        self.new_states = torch.tensor(np.zeros((self.size, self.obs_dim)), dtype=torch.float32, device=device)
        self.rewards = torch.tensor(np.zeros(self.size), dtype=torch.float32, device=device)
        self.actions = torch.tensor(np.zeros(self.size), dtype=torch.int32, device=device)
        self.terminal = torch.tensor(np.zeros(self.size), dtype=torch.bool, device=device)
        self.current_size = 0
        self.index = 0

class DQN_learner:

    def __init__(self, env, main_network, target_network, replay, 
                 device=torch.device('cpu')):
        self.env = env 
        self.main_network = main_network
        self.target_network = target_network
        self.copy_weights()
        self.replay = replay
        self.device = device

    def learn(self, num_episodes=1000, gamma=0.90, train_interval=10, copy_interval=100, 
              epsilon=lambda x: 0.1):

        env = gym.wrappers.RecordEpisodeStatistics(self.env, deque_size=num_episodes)

        step_count = 0
        print('Running training episodes.')
        for episode in tqdm(range(num_episodes)):

            state, _ = env.reset()
            terminal = False
            truncated = False

            while not terminal and not truncated:

                # Choose action
                # If random number is less than epsilon, then select a random action
                # Else select according to the Q-values. When selecting according to Q-values,
                if np.random.rand() < epsilon(episode):
                    action = env.action_space.sample()
                else:
                    state_tensor = torch.tensor(state, dtype=torch.float32, device=self.device).unsqueeze(0)
                    with torch.no_grad():
                        Q = self.main_network(state_tensor)
                        action = torch.argmax(Q).item()

                # Take action by calling env.step
                next_state, reward, terminal, truncated, info = env.step(action)
                step_count += 1

                # Add to replay buffer
                self.replay.add(state, action, reward, next_state, terminal)

                # Train the network. 
                if step_count % train_interval == 0 and step_count > 200:
                    self.train_network(gamma=gamma)

                # Copy weights from the main network to the target network
                if step_count % copy_interval == 0:
                    self.copy_weights()        

                # Update the state. 
                state = next_state

        return env.return_queue, env.length_queue

    def train_network(self, gamma=0.90):
        # Get a batch of data from the replay buffer
        states, actions, rewards, new_states, terminal = self.replay.get_batch()
        batch_size = len(states)

        # Calculate the Q-values for the current state
        Q = self.main_network(states)

        # Calculate the Q-values for the next state
        with torch.no_grad():
            Q_next = self.target_network(new_states).detach()
            Q_next_max = torch.max(Q_next, dim=1).values

        # Calculate the TD-target - Remember to use the terminal states such that 
        # the target for terminal states is just the reward. 
        # torch.logical_not is useful here. 
        td_target = rewards + torch.logical_not(terminal) * gamma * Q_next_max

        # Calculate the loss
        Q = Q[torch.arange(batch_size), actions]
        loss = torch.nn.functional.mse_loss(Q, td_target)

        # Backpropagate the loss
        loss.backward()
        self.main_network.optimizer.step()
        self.main_network.optimizer.zero_grad()

    def test(self, N=100):

        env = gym.wrappers.RecordEpisodeStatistics(self.env, deque_size=N)
        print('Running test episodes')
        for episode in tqdm(range(N)):

            state, _ = env.reset()
            terminal = False
            truncated = False

            while not terminal and not truncated:

                state_tensor = torch.tensor(state, dtype=torch.float32, device=self.device).unsqueeze(0)
                with torch.no_grad():
                    Q = self.main_network(state_tensor)
                    action = torch.argmax(Q).item()

                # Take action by calling env.step
                next_state, reward, terminal, truncated, info = env.step(action)

                # Update the state. 
                state = next_state

        return np.array(env.return_queue).flatten(), np.array(env.length_queue).flatten()

    def copy_weights(self):
        self.target_network.load_state_dict(deepcopy(self.main_network.state_dict()))

def rolling_average(x, N):
    return np.convolve(x, np.ones(N), 'same') / N

def random_rollouts(env, N=100):
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=N)
    print('Running random rollouts')
    for episode in tqdm(range(N)):
        state, _ = env.reset()
        terminal = False
        truncated = False
        while not terminal and not truncated:
            action = env.action_space.sample()
            next_state, reward, terminal, truncated, info = env.step(action)
            state = next_state
    return np.array(env.return_queue).mean(), np.array(env.length_queue).mean()

if __name__ == '__main__':

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print('Using device:', device)

    env = gym.make('CartPole-v1')
    #env = gym.make('Blackjack-v1')
    #env = gym.make('FrozenLake-v1', is_slippery=False)

    obs_dim = 4
    n_actions = 2

    network_settings = {'hidden_dim': 64, 'learning_rate': 1e-3, 'hidden_layers':3}

    main_network = Q_network(observation_dim=obs_dim, action_dim=n_actions, **network_settings).to(device)
    target_network = Q_network(observation_dim=obs_dim, action_dim=n_actions, **network_settings).to(device)
    replay = ExperienceReplay(observation_dim=obs_dim, size=1000, batch_size=64, device=device)

    learner = DQN_learner(env, main_network, target_network, replay, device=device)
    num_episodes = 500
    epsilon = lambda i: 1 - (i / num_episodes) * 0.9
    #epsilon = lambda i: 0.1


    returns, lengths = learner.learn(num_episodes=num_episodes, gamma=0.99, train_interval=1, copy_interval=100, epsilon=epsilon)
    
    num_episodes_test = 100
    test_returns, test_lengths = learner.test(N=num_episodes_test)

    fig, ax = plt.subplots(2, 2, figsize=(8, 8))

    returns = np.array(returns).flatten()
    lengths = np.array(lengths).flatten()

    random_returns, random_lengths = random_rollouts(env, N=num_episodes)

    ax[0, 0].plot(returns, alpha=0.5)
    ax[0, 0].plot(rolling_average(returns, 100), linewidth=3)
    ax[0, 0].plot([0, num_episodes], [random_returns, random_returns], linewidth=3)
    ax[0, 0].set_xlabel('Episode')
    ax[0, 0].set_ylabel('Return')

    ax[0, 1].plot(lengths, alpha=0.5)
    ax[0, 1].plot(rolling_average(lengths, 100), linewidth=3)
    ax[0, 1].plot([0, num_episodes], [random_lengths, random_lengths], linewidth=3)
    ax[0, 1].set_xlabel('Episode')
    ax[0, 1].set_ylabel('Length')

    ax[1, 0].plot(test_returns, alpha=0.5)
    ax[1, 0].plot([0, num_episodes_test], np.array([1, 1]) * np.mean(test_returns), linewidth=3)
    ax[1, 0].plot([0, num_episodes_test], [random_returns, random_returns], linewidth=3)
    ax[1, 0].set_xlabel('Episode')

    ax[1, 1].plot(test_lengths, alpha=0.5)
    ax[1, 1].plot([0, num_episodes_test], np.array([1, 1]) * np.mean(test_lengths), linewidth=3)
    ax[1, 1].plot([0, num_episodes_test], [random_lengths, random_lengths], linewidth=3)
    ax[1, 1].set_xlabel('Episode')

    plt.tight_layout()
    plt.savefig('test.png')