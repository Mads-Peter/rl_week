import torch
import numpy as np

def fill_replay(replay_class, size=100, batch_size=10):
    replay = replay_class(size=size, batch_size=batch_size)

    for i in range(10):
        state = np.array([i, i])
        action = i % 4
        reward = np.random.randint(low=0, high=1)
        new_state = np.array([i+1, i+1])
        terminal = i % 10 == 0

        replay.add(state, action, reward, new_state, terminal)

    return replay
    
def test_replay(replay_class):

    replay = fill_replay(replay_class, size=100, batch_size=10)

    assert replay.states.shape[0] == 100, 'states.shape[0] != 100'
    assert replay.actions.shape[0] == 100, 'actions.shape[0] != 100'
    assert replay.rewards.shape[0] == 100, 'rewards.shape[0] != 100'
    assert replay.new_states.shape[0] == 100, 'new_states.shape[0] != 100'
    assert replay.terminal.shape[0] == 100, 'terminal.shape[0] != 100'

    batch = replay.get_batch()
    state_batch, action_batch, reward_batch, new_state_batch, terminal_batch = batch

    assert state_batch.shape[0] == 10, 'state_batch.shape[0] != 10'
    assert action_batch.shape[0] == 10, 'action_batch.shape[0] != 10'
    assert reward_batch.shape[0] == 10, 'reward_batch.shape[0] != 10'
    assert new_state_batch.shape[0] == 10, 'new_state_batch.shape[0] != 10'
    assert terminal_batch.shape[0] == 10, 'terminal_batch.shape[0] != 10'

    indices = state_batch[:, 0].detach().numpy().astype(int)
    assert (replay.rewards[indices] == reward_batch).all(), 'Rewards dont match.'
    assert (replay.states[indices] == state_batch).all(), 'States dont match.'
    assert (replay.actions[indices] == action_batch).all(), 'Actions dont match.'
    assert (replay.new_states[indices] == new_state_batch).all(), 'New states dont match.'
    assert (replay.terminal[indices] == terminal_batch).all(), 'Terminal dont match.'

def test_train(train_func, replay_class, network_class):

    network_1 = network_class()
    network_2 = network_class()
    
    replay = fill_replay(replay_class, size=10, batch_size=10)

    for gamma in [0, 0.9, 1.0]:

        np.random.seed(0)
        states, actions, rewards, new_states, terminal = replay.get_batch()

        Q = network_1.forward(states).detach().numpy()

        Q_next = network_2.forward(new_states).detach().numpy()

        loss = []
        for i in range(10):

            if not terminal[i]:
                Q_target = rewards[i] + gamma * np.max(Q_next[i])
            else:
                Q_target = rewards[i]


            loss.append((Q[i, actions[i]] - Q_target)**2)
        
        loss = np.mean(loss)
        
        np.random.seed(0)
        l = train_func(replay, network_1, network_2, gamma=gamma)

        diff = np.abs(l - loss)

        assert diff < 1e-4, f'Losses dont match with gamma={gamma}. Expected loss {loss:0.6f}, got {l:0.6f}.'


def simplify_desc(desc):
    for i in range(desc.shape[0]):
        for j in range(desc.shape[1]):
            try:
                desc[i, j] = desc[i, j].decode('utf-8')
            except:
                desc[i, j] = desc[i, j]
    return desc

def full_replay(replay_class, env, desc=None):

    env.reset()

    if desc is not None:
        desc = simplify_desc(desc)

    replay = replay_class(size=64-5*4, batch_size=64-5*4)

    for row in range(4):
        for col in range(4):

            add_actions = True
            if desc is not None:
                if desc[row, col] == 'H':
                    add_actions = False
                elif desc[row, col] == 'G':
                    add_actions = False

            if add_actions:
                for a in range(4):
                    env.state = (row, col)
                    state = env.state
                    next_state, reward, terminal, _ = env.step(a)
                    replay.add(state, a, reward, next_state, terminal)
                    env.reset()

    print(len(replay.states))

    return replay

def reduced_replay(replay_class, env, desc=None):

    replay = replay_class(size=8, batch_size=8)

    for row, col in zip([2, 3], [2, 2]):

        print(row, col)

        for a in range(4):
            env.state = (row, col)
            state = env.state
            next_state, reward, terminal, _ = env.step(a)
            replay.add(state, a, reward, next_state, terminal)
            env.reset()

    return replay    









    

    