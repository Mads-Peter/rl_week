# Running on Google Colab.

Open one of the notebooks on Google Colab. 

Add this line at the very top and run it: 

```
!pip install git+https://gitlab.com/Mads-Peter/rl_week.git
```

This should install rather fast, and you can now work on the notebook in Colab. 
Remember to save to your cloud or download if you want to keep the progress. 

# Installation: 

Start by cloning the repository, 

```
git clone https://gitlab.com/Mads-Peter/rl_week.git
```

Move into the directory with, `cd rl_week`
I suggest making a new virtual environment, if not on Grendel ignore the ml commands, do something like

```
deactivate
ml purge
ml python/3.9.4
mkdir env
cd env
python3 -m venv rl_week
cd ..
```

Then source the environment: 

```
source env/rl_week/bin/activate
```

Upgrade pip with 

```
pip install --upgrade pip
```

Now you can use pip to install the requirements and the `rlweek` package itself

```
pip install -e . 
```

# Running notebooks on Grendel. 

Make sure to source your environment, 

```
source env/rl_week/bin/activate
```

Start a jupyter notebook

```
jupyter notebook
```

This will write something like 

```
[I 2023-08-14 15:20:52.505 ServerApp] Jupyter Server 2.7.0 is running at:
[I 2023-08-14 15:20:52.505 ServerApp] http://localhost:8889/tree
[I 2023-08-14 15:20:52.505 ServerApp]     http://127.0.0.1:8889/tree
```
Here the 8889 is a port, you need to forward that port to your machine
so replace <PORT> in the below with those numbers and run

```
ssh <YOUR_LOGIN>@grendel.cscaa.dk -g -L8080:127.0.0.1:<PORT> -N
```

Then you can open it with this line in your browser:

```
localhost:<LOCAL_PORT>
```
And it should open a jupyter interface. 

### Running on a node. 


If the for some reason the frontend is slow, then we can run on nodes instead:

```
srun --pty --x11 -p q48,q40,q36 -c 2 --time 10:00:00 /bin/bash 
```

Then run 

```
echo ssh $USER@grendel.cscaa.dk -g -L8080:$SLURMD_NODENAME.grendel.cscaa.dk:40000 -N
```

That gives you the command you will need to forward the notebook instance, that you start
with 

```
jupyter notebook --port=40000 --ip=$SLURMD_NODENAME.grendel.cscaa.dk
```

Then in a new terminal run, the command that resulted from the echo above, e.g. 
for me: 

```
ssh machri@grendel.cscaa.dk -g -L8080:s95n54.grendel.cscaa.dk:40000 -N
```

And you can access the notebooks in your browser at `localhost:8080`.

You can get an interactive node with GPU with 

```
srun --pty --x11 -p qgpu --gres=gpu:1 --time 02:00:00  /bin/bash
```
You check check that pytorch detects a GPU in python with 

```
import torch
torch.cuda.is_available()
```

Though I haven't been able to make this work with notebooks due to some SSL differences on gpu nodes (?). 
So when using GPU you might need to work with scripts rather than notebooks, which might 
be preferable when tinkering with something that needs GPU anyway. 


# Tutorials: 

![rl_taxonomy](rl_taxonomy.png)

The above figure, that I have taken from [Spinning Up](https://spinningup.openai.com/en/latest/spinningup/rl_intro2.html),
gives an approximate taxonomy of RL algorithms. We will be focussed on the *model-free* branch, as I think these are 
the essential algorithms and the *model-based* approach often use parts from these. 
While the terminology might not be clear you yet (it will be explained in the first notebook), a model in this context is something that approximates 
the state-transition and reward function of an RL environment. That it is, it predicts 
what state an action results in and what the reward for that transition is. In the model-free 
approaches, the algorithms do not attempt to directly learn this model, but just learn how 
to act in such a way that the cumulative reward is maximized. Whereas in the model-based 
approaches this is model is learned or given, e.g. to do planning using the model (like thinking ahead in chess). 

So the notebook tutorials consists of three main topics

- Notebooks focused on Q-learning, these are in `rlweek/q_learning`
    - tutorial01_q_learning.ipynb: First introduction to RL with tabular Q-learning. 
    - tutorial02_q_learning_gym.ipynb: Deep Q-networks in `gymnasium` environments. 
- Notebooks focused on policy-gradient learning in `rl_week/policy_learning`
    - tutorial03_policy_learning.ipynb: Coding the simplest policy gradient method REINFORCE for a simple environment, and upgrading it with a simple change. 
    - tutorial04_network_policy.ipynb: REINFORCE with neural networks for a more difficult environment. 
- Notebooks on a custom environment for atomistic structure optimization in `rlweek/atomic_environment`
    - tutorial05_atomistic_environment.ipynb: Definition and some exploration of an environment that can be used for atomistic optimization. Note: This has no clear tasks, but will be the starting point for playing around with RL algorithms for optimization. 
    - tutorial06_equivariance.ipynb: Equivariance is nice, as it allows the prediciton of displacements. Here I show that `SchNetPack` can be used to create networks that can handle prediction of displacements. 

Some notebooks contain optional exercises, it is probably best to leave those 
for when you have finished both notebooks on that topic. 

The most natural order to work through this is according to the numbering. 
The last two are as mentioned dont contain clear exercises, but outline a 
way of working with RL for atomistic optimization - with the goal being that 
we manage to implement some of the algorithms (or related) ones from the first 
four notebooks to solve. 

# Resources: 

- The bible of reinforcement learning is [Sutton & Barto](http://incompleteideas.net/book/the-book-2nd.html)
- An excellent youtube series on RL by [Mutual Information](https://www.youtube.com/watch?v=NFo9v_yKQXA&list=PLzvYlJMoZ02Dxtwe-MmH4nOB5jYlMGBjr&ab_channel=MutualInformation)
- Concise write-ups on Lil'Log by Lilian Weng covering an [overview on RL](https://lilianweng.github.io/posts/2018-02-19-rl-overview/), a long post on [policy gradient methods](https://lilianweng.github.io/posts/2018-04-08-policy-gradient/) and even some [implementations](https://lilianweng.github.io/posts/2018-05-05-drl-implementation/#actor-critic) off some algorithms. 
- There exists a variety of Towards Data Science, Medium etc articles on RL - but they often skip the most difficult steps in their explanations so don't be surprised if you dont learn much from them. 
- [Spinning Up](https://spinningup.openai.com/en/latest/index.html) contains a nice but mathy introduction to policy gradient methods. 
- Original papers for algorithms can be helpful, but honestly they often express themselves in the most complicated way possible for their given algorithm - perhaps this increases the chance of being accepted for NIPS. You can look them up if you want to. 
- The internet, with the correct search terms it is often possible to find good questions an answers from e.g. stack exchange on RL topics. 

# Code libraries

There are several rather good code libraries containing implementations of many of the best RL algorithms, these 
are usually written to interface with `gynamsium` environments. These are important to keep in mind 
for several reasons, in some cases they might provide an implementation that can be taken off the shelf, 
in other cases it might be nice to benchmark your own implementation against a known good implementation, 
and have access to the code of their implementations is also nice for debugging to see if some 
concept has been misunderstood. 

- [StableBaselines](https://github.com/DLR-RM/stable-baselines3) has implementations of many algorithms. 
- [CleanRL](https://github.com/vwxyzjn/cleanrl) have particularly clean and self-contained codes for many algorithms, that makes it easier to read and perhaps use as a starting point for a tweaked version for your use case. 
- [Spinning up](https://spinningup.openai.com/en/latest/index.html) has implementations of several Policy gradient methods. 
- [Gymnasium](https://gymnasium.farama.org/) The standard for implementing environments. 
- [RLlib](https://docs.ray.io/en/latest/rllib/index.html) This is from the AnyScale team, that also are the developers of Ray, and advertise 'Industrial grade RL', this library offers MANY algorithms but it is not too easy to get started with but probably 
one of the highest performing libraries. 
- [Acme](https://github.com/deepmind/acme) developed by Deepmind provides a toolbox for building agents (Though I have not really tried it)
- [TorchRL](https://pytorch.org/rl/tutorials/torchrl_demo.html) Official Pytorch implementations of various pieces of RL algorithms, and some good looking [tutorials](https://pytorch.org/rl/tutorials/coding_ppo.html). 

There are many many more, which is great, and I think using some of the libraries in some capacity is 
the way to go with reinforcement learning. My general suggestion is 

- Use CleanRL, StableBaselines and Spinning Up as references on how to implement things if something is not working. 
- Use CleanRL or StableBaseLines to easily benchmark against if that is necessary. 
- If top performance is a goal then RLlib would probably be the place to look. 
- If developing something more complicated it might be nice to try TorchRL or Acme, to have a standard (think AbstractBaseClass) to work in. 
